import startMongo from './startMongo';
import { startServer } from './startServer';

export const runBootTasks = async () => {
  try {
    await startMongo();
    await startServer();
    console.log('Boot succeed!');
  } catch (error) {
    console.log('run boot tasks error', error);
  }
};
