export interface IError {
    code: string;
    message: string;
    details?: Record<string, unknown>;
}