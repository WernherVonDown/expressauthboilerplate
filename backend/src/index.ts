import { runBootTasks } from './boot/index';

async function start() {
  await runBootTasks();
}

start().catch((e) => {
  console.log(e);
  process.exit();
});
