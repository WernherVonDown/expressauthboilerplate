import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import router from '../router';
import { errorMiddleware } from '../middlewares/error.middleware';
import { vars } from './vars';

const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(cors({
  credentials: true,
  origin: vars.clientUrl,
}));
app.use('/api', router);

app.use(errorMiddleware);

export { app };
