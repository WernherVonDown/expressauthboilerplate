import dotenv from 'dotenv';

dotenv.config();

export const vars = Object.freeze({
  PORT: process.env.PORT || 5000,
  apiUrl: process.env.API_URL || 'http://localhost:8000',
  clientUrl: process.env.CLIENT_URL || 'http://localhost:3000',
  mongo: {
    url: process.env.MONGO_URL || 'mongodb://mongo:27017/database?replicaSet=rs0',
  },
  jwt: {
    accessSecret: process.env.JWT_ACCESS_SECRET || 'access_secret',
    refreshSecret: process.env.JWT_REFRESH_SECRET || 'refresh_secret',
  },
  email: {
    service: process.env.EMAIL_SERVICE || 'GMAIL',
    gmail: {
      user: process.env.GMAIL_USER,
      pass: process.env.GMAIL_PASS,
    },
    jwt: {
        accessSecret: process.env.JWT_ACCESS_SECRET || 'access_secret',
        refreshSecret: process.env.JWT_REFRESH_SECRET || 'refresh_secret',
    },
    email: {
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD,
        },
    },
})
