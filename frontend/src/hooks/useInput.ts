import { useCallback, useState } from "react"

export const useInput = (initialValue: string) => {
    const [value, setValue] = useState(initialValue);

    const onChange = useCallback((e: any) => {
        setValue(e.target.value)
    }, []);

    return {
        onChange,
        value,
    }
}