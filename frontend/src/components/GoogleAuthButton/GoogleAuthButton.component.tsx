import { Button, IconButton, SvgIcon, Typography } from '@mui/material';
import classNames from 'classnames';
import React, { useContext } from 'react';
import GoogleLogin from 'react-google-login';
import { config } from '../../config/config';
import { AuthContext } from '../../context/AuthContext';
import GoogleIcon from '../../icons/GoogleIcon';
import styles from './GoogleAuthButton.module.scss';
// import GoogleIcon from '@mui/icons-material/Google';

const GoogleAuthButton = () => {
    const { actions: { googleAuth } } = useContext(AuthContext);

    const onSuccess = (res: any) => {
        const {profileObj: {givenName, email}, accessToken} = res;
        googleAuth(givenName, email, accessToken)
    }
    return (
        <GoogleLogin
            clientId={config.googleAuthClientId}
            render={renderProps => (
                <Button
                    onClick={renderProps.onClick}
                    variant={'contained'}
                    className={classNames(styles.googleBtn)}
                >
                    <SvgIcon fontSize="medium"><GoogleIcon /></SvgIcon>
                    <Typography>Войти с Google</Typography>
                </Button>
            )}
            buttonText="Login"
            onSuccess={onSuccess}
            onFailure={(e) => console.log("ON FAIL", e)}
            cookiePolicy={'single_host_origin'}
        />
    )
}

export default GoogleAuthButton;