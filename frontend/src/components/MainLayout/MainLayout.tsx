import React, { ReactNode, useContext } from "react";
import { AppBar, Box, Button, Container, styled, Toolbar, Typography } from "@mui/material";
import Link from "next/link";
import { AuthContext } from "../../context/AuthContext";
import { LoginRoute, RegistrationRoute } from "../../const/APP_ROUTES";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[]
}

export const MainLayout: React.FC<IProps> = ({ children }) => {
    const { state: { loggedIn }, actions: { logout } } = useContext(AuthContext);
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        <Link href="/">Project</Link>
                    </Typography>
                    {!loggedIn ? <>
                        <Button href={RegistrationRoute} color="inherit" variant='text'>
                            Регистрация
                        </Button>
                        <Button href={LoginRoute} color="inherit" variant='outlined'>
                            Вход
                        </Button>
                    </> : <>
                        <Button onClick={logout} color="inherit" variant='outlined'>
                            Выйти
                        </Button>
                    </>
                    }

                </Toolbar>
            </AppBar>
            <Offset />
            <main>
                {children}
            </main>
        </>
    )
}