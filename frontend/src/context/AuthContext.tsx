import React, { ReactElement, useMemo } from 'react';
import { IUser } from '../const/user/types';
import { useState, useCallback } from 'react';
import AuthService from '../services/authService';
import { API_URL } from '../http/index';
import axios from 'axios';
import { UserApiRoutes } from '../const/user/API_ROUTES';


interface IState {
    loggedIn: boolean;
    user: IUser | undefined;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const AuthContext = React.createContext({} as ContextValue);

const AuthContextProvider = (props: IProps) => {
    const { children } = props;
    const [loggedIn, setLoggedIn] = useState<boolean>(false);
    const [user, setUser] = useState<IUser>();

    const login = useCallback(async (email: string, password: string) => {
        try {
            const response = await AuthService.login(email, password);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
        } catch (error) {
            console.log('login error;', error);
        }
    }, [])

    const googleAuth = async (username: string, email: string, accessToken: string) => {
        try {
            const response = await AuthService.googleAuth(email, username, accessToken);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            return { success: true };
        } catch (error) {
            console.log('googleAuth error;', error)
            return { success: false };
        }
    }

    const logout = useCallback(async () => {
        try {
            const response = await AuthService.logout();
            localStorage.removeItem('token');
            setLoggedIn(false);
        } catch (error) {
            console.log('logout error;', error)
        }
    }, [])

    const register = useCallback(async (email: string, password: string): Promise<{success: boolean}> => {
        try {
            const response = await AuthService.registration(email, password);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            return { success: true };
        } catch (error) {
            console.log('register error;', error)
            return { success: false };
        }
    }, []);

    const resetPasswordConfirm = useCallback(async (email: string, token: string, password: string) => {
        try {
            const response = await AuthService.resetPasswordConfrim(email, token, password);
            return { success: true };
        } catch (error) {
            console.log('resetPasswordConfirm error;', error);
            return { success: false };
        }
    }, []);


    const resetPassword = useCallback(async (email: string) => {
        try {
            const response = await AuthService.resetPassword(email);
            return { success: true };
        } catch (error) {
            console.log('resetPassword error;', error);
            return { success: false };
        }
    }, []);

    const checkAuth = useCallback(async () => {
        try {
            const response = await axios(`${API_URL}/${UserApiRoutes.REFRESH}`, { withCredentials: true });
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
        } catch (error) {
            console.log('checkAuth error;', error)
        }
    }, [])

    const state = useMemo(() => ({
        loggedIn,
        user,
    }), [
        loggedIn,
        user,
    ]);

    const actions = {
        login,
        logout,
        register,
        checkAuth,
        resetPassword,
        resetPasswordConfirm,
        googleAuth,
    }


    return <AuthContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </AuthContext.Provider>
}

AuthContextProvider.defaultProps = {
    state: {
        loggedIn: false,
    }
}


export { AuthContext, AuthContextProvider };
