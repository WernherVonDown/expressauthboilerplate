import React, { ReactElement, useMemo } from 'react';
import { responsiveFontSizes } from '@mui/material';
import { ThemeProvider, createTheme, Theme } from '@mui/material/styles';

interface IState {
    theme: Theme;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const ThemeContext = React.createContext({} as ContextValue);

const ThemeContextProvider = (props: IProps) => {
    const { children } = props;

    const theme = useMemo(() => {
        const theme = createTheme({palette: {
            mode: 'light',
            primary: {
                contrastText: "#fff",
                dark: "#ab4848",
                light: "#e79191",
                main: "#e96565",
            }
          }});
        return responsiveFontSizes(theme);
    }, [])

    const state = useMemo(() => ({
        theme,
    }), [
        theme,
    ]);

    const actions = {
    }

   


    return <ThemeContext.Provider
        value={{
            state,
            actions
        }}
    >
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    </ThemeContext.Provider>
}

ThemeContextProvider.defaultProps = {
    state: {
    }
}


export { ThemeContext, ThemeContextProvider };
