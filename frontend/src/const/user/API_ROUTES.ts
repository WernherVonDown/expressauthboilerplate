const BASE = 'user'

export const UserApiRoutes = {
    LOGIN:`${BASE}/login`,
    REGISTRATION:`${BASE}/registration`,
    LOGOUT:`${BASE}/logout`,
    REFRESH:`${BASE}/refresh`,
    RESET_PASSWORD: `${BASE}/reset-password`,
    RESET_PASSWORD_CONFIRM: `${BASE}/reset-password-confirm`,
    GOOGLE_AUTH: `${BASE}/googleAuth`,
}