/** @type {import('next').NextConfig} */

require('dotenv').config();

const publicRuntimeConfig = {
  apiUrl: process.env.NEXT_PUBLIC_API_URL || "http://localhost:8000/api",
  googleAuthClientId: process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID || '',
}


const nextConfig = {
  publicRuntimeConfig,
  reactStrictMode: false,
}

module.exports = nextConfig
